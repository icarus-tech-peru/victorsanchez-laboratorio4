package com.icarus.laboratorio4

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.popup.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val marcas= listOf("KIA","DATSUN","TOYOTA","RENAULT","FERRARI")
        val adap=ArrayAdapter(this,R.layout.style_item,marcas)
        spMarcas.adapter=adap

        btnProcesar.setOnClickListener {
            val titular:String=edtTitular.text.toString()

            val sp:Spinner=findViewById(R.id.spMarcas)
            val marca=sp.selectedItem.toString()

            val anno:String=edtAnno.text.toString()
            val descripcion:String=edtDescripcion.text.toString()

            if(titular.isEmpty()){
                Toast.makeText(this,"Debe ingresar el nombre del titular",Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if(anno.isEmpty()){
                Toast.makeText(this,"Debe ingresar el año del vehiculo",Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if(descripcion.isEmpty()){
                Toast.makeText(this,"Debe ingresar alguna descripcion",Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            val auto=Vehiculo(titular,marca,anno,descripcion)
            crearVentanaModal(auto.titular,auto.marca,auto.anno,auto.descripcion).show()
        }
    }

    fun crearVentanaModal(titular:String,marca:String,anno:String,problema:String):AlertDialog{
        val alertdialog:AlertDialog
        val builder=AlertDialog.Builder(this)

        val view=layoutInflater.inflate(R.layout.popup,null)
        builder.setView(view)

        alertdialog=builder.create()

        val botonaceptar:Button=view.findViewById(R.id.btnAceptar)
        val texto:TextView=view.findViewById(R.id.tvResumen)
        texto.text="Datos enviados: Vehiculo marca:$marca, titular:$titular, año:$anno, problema registrado:$problema"

        botonaceptar.setOnClickListener {
            alertdialog.dismiss()
        }

        return alertdialog
    }


}